#!/usr/bin/env node

'use strict';

const fs = require('fs-extra');

const yargs = require('yargs');

const PathUtil = require('./PathUtil');
const ArgumentsProcessor = require('./ArgumentsProcessor');
const ExtClass = require('./ExtClass');
const FutureExtClass = require('./FutureExtClass');

var argv = yargs
    .usage('Usage: $0 [sourceFile] [targetFile]')
    .option('force', {
        alias: 'f',
        describe: 'Create directories that do not exist'
    })
    .option('dry', {
        alias: 'd',
        describe: 'Dry run to print results without making changes'
    })
    .demand(2)
    .check(ArgumentsProcessor.process)
    .argv;


var sourcePath = argv._[0];
var targetPath = argv._[1];
var dryRun = argv.dry;

var studioPath = PathUtil.getStudioPath(targetPath);
var targetStudioPath = PathUtil.getRelativePath(targetPath, studioPath);

var sourceExtClass = new ExtClass(sourcePath);
var targetExtClass = new FutureExtClass(targetStudioPath);

// Update source code
var newText = sourceExtClass.getText()
    .replace(new RegExp(sourceExtClass.getClassName(), 'g'), targetExtClass.getClassName())
    .replace(/Ext\.namespace\(".*"\)/g, 'Ext.namespace("' + targetExtClass.getNamespace() + '")');

if (dryRun) {
    console.log(newText);
} else {
    // Move file
    fs.writeFileSync(targetPath, newText);
    fs.unlinkSync(sourcePath);
}
