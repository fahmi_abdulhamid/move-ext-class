# move-ext-class

CLI utility to move an ExtJs class file from one directory to another. Will automatically update the file with the new namespace and class name.

## Setup
1. Clone repo
2. cd into dir
3. Run ```npm install``` to resolve dependencies
4. Run ```npm link``` to add ```move-ext-class``` to path

## Usage

```
Usage: move-ext-class [sourceFile] [targetFile]

Options:
  --force, -f  Create directories that don't exist
```

## Future work
- Update build.jsb files
