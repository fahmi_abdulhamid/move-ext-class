'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
    ensureFileName: function (dirPath, examplePath) {
        if (this.isDirPath(dirPath)) {
            dirPath = path.join(dirPath, path.basename(examplePath));
        }

        return dirPath;
    },

    getDirPath: function (filePath) {
        return path.dirname(filePath);
    },

    pathExists: function (path) {
        return fs.existsSync(path);
    },

    fileExists: function (path) {
        return this.pathExists(path) && fs.lstatSync(path).isFile();
    },

    isDirPath: function (path) {
        return path.match(/(\.\w+$)/gm) == null;
    },

    getStudioPath: function (dirPath) {
        const STUDIO_ROOT_FILE = 'studio.iml';

        // Does not work for Windows
        while (dirPath !== '/' && !fs.existsSync(path.join(dirPath, STUDIO_ROOT_FILE))) {
            dirPath = path.resolve(dirPath, '..');
        }

        if (dirPath === '/') {
            dirPath = null;
        }

        return dirPath;
    },

    getRelativePath: function (fromPath, toPath) {
        var fromAbsolutePath = path.resolve(fromPath);
        var relativePath = path.relative(toPath, fromAbsolutePath);
        return relativePath;
    }
};
