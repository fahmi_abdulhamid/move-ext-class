'use strict';

const fs = require('fs-extra');

const PathUtil = require('./PathUtil');

module.exports = {
    process: function (argv) {
        var sourcePath = argv._[0];
        var targetPath = argv._[1];
        var force = argv.force;

        if (!PathUtil.fileExists(sourcePath)) {
            throw new Error('Source file does not exist');
        }

        var studioPath = PathUtil.getStudioPath(targetPath);
        if (studioPath == null) {
            throw new Error('Target path is not in Studio');
        }

        targetPath = PathUtil.ensureFileName(targetPath, sourcePath);
        argv._[1] = targetPath;

        var targetDir = PathUtil.getDirPath(targetPath);
        if (!PathUtil.pathExists(targetDir)) {
            if (force) {
                fs.mkdirsSync(targetDir);
            } else {
                throw new Error('Target directory does not exist. Use -f or --force to create it');
            }
        }

        // Validation success
        return true;
    }
};
