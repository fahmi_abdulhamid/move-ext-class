'use strict';

const path = require('path');

var FutureExtClass = function (path) {
    this._path = path;
};

FutureExtClass.prototype = {
    getClassName: function () {
        var pathParts = this._path.split(path.sep);

        var namespaceParts = ['Studio']
            .concat(pathParts.slice(2))
            .map(function (namespacePart) {
                return namespacePart.replace(/-/g, '');
            });

        var className = namespaceParts
            .join('.')
            .replace(/\.js$/, '');

        return className;
    },

    getNamespace: function () {
        var className = this.getClassName();
        var namespace = this._generateNamespaceFromClassName(className);
        return namespace;
    },

    _generateNamespaceFromClassName: function(className) {
        return className.replace(/\.\w+$/, '');
    }
};

module.exports = FutureExtClass;
