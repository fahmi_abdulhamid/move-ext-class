'use strict';

const fs = require('fs');

var ExtClass = function (path) {
    this._path = path;
};

ExtClass.prototype = {
    getClassName: function () {
        var text = this.getText();
        var className = text.match(/([\w\.]+)\s*=\s*function/)[1];
        return className;
    },

    getNamespace: function () {
        var className = this.getClassName();
        var namespace = this._generateNamespaceFromClassName(className);
        return namespace;
    },

    getText: function () {
        var text = fs.readFileSync(this._path).toString();
        return text;
    },

    _generateNamespaceFromClassName: function(className) {
        return className.replace(/\.\w+$/, '');
    }
};

module.exports = ExtClass;
